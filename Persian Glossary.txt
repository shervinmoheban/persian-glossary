en:fa 
Login:ورود 
Close:بستن 
Previous:قبلی 
Next:بعدی 
Previous month:ماه قبلی 
First:اولین 
Next month:ماه بعدی 
Last:آخرین 
Edit:ویرایش 
Editor:ویرایشگر 
Upload:بارگذاری 
Download:بارگیری 
Get help:کمک گرفتن
Subscribed:مشترک شده‌اید
Unsubscribe:لغو اشتراک
User:کاربر
password:گذرواژه
Username:نام‌کاربری
Username:شناسه
Username:شناسه کاربری
Signup:ثبت‌نام
Filters:فیلتر‌ها
Profile:نمایه
Profile:مشخصات
Log Out:خروج
Subscriptions:اشتراک
Subscriptions:اشتراکی
Overview:نمای‌کلی
Trending:مورد بحث
Trends:موضوع‌های داغ
Trends:بحث‌های داغ
Recently added:به تازگی اضافه شده
More:دیگر
About:درباره
Search:جستجو
Sort:براساس
Published date:تاریخ انتشار
Duration:مدت زمان
Yes:بله
No:خیر
Category:دسته‌بندی
Licence:مجوز
Language:زبان
No results:بدون نتیجه
Description:توضیحات
Import:وارد کردن
Import to:وارد کردن به
Users:کاربران
Administrator:مدیر
Twitter:توییتر
Facebook:فیسبوک
Your Twitter username:نام‌کاربری توییتر شما
Reset my password:تغییر گذرواژه‌ام
No results found:نتیجه‌ای یافت نشد
My public profile:نمایه عمومی من
My account:حساب کاربری من
Configuration:تنظیمات
Score:امتیاز
Host:میزبان
Followers:دنبال‌کنندگان
Followers:دبنال‌کننده‌ها
Create user:ایجاد کاربر
Role:نقش
Users list:لیست کاربران
Reason:دلیل
Ban reason:دلیل محدودیت
Video:ویدئو
Videos:ویدئو‌ها
Videos:ویدئو‌های
Movies:فیلم‌ها
Movies:فیلم‌های
Movie:فیلم
My:من
Settings:تنظیمات
cancel:لغو
status:وضعیت
Select month:انتخاب ماه
Select:انتخاب
Month:ماه
Select year:انتخاب سال
Year:سال
Hours:ساعات
Hours:ساعت‌ها
Hour:ساعت
Minutes:دقایق
Minutes:دقیقه‌ها
Minutes:دقیقه
Minute:دقیقه
Seconds:ثانیه
Seconds:ثانیه‌ها
PM:ب.ظ
AM:ق.ظ
Delete:حذف
Remove:حذف
Remove:برداشتن
or:یا
Email address:آدرس رایانامه
Email address:رایانامه
Email address:نشانه رایانامه
or create an account:یا یک حساب بسازید
Create:ساختن
Create:ساخت
Password:گذرواژه
I forgot my password:گذرواژه‌ام را فراموش کرده‌ام
I:من
Forgot:فراموش
My Passsword:گذرواژه‌ام
Forgot your password:گذرواژه‌تان را فراموش کرده‌اید
Forgot your password:گذرواژه‌‌ی خودتان را فراموش کرده‌اید
Email:رایانامه
Send me an email to reset my password:یک رایانامه برای بازنشانی گذرواژه برای من بفرست
Send me an email to reset my password:یک رایانامه برای بازیابی گذرواژه برای من بفرست
Send:ارسال
Reset:بازنشانی
Reset:تغییر
Restore:بازیابی
Me:من
Reset my password:تغییر گذرواژه‌ام
Reset my password:تغییر گذرواژه‌ی من
Reset my password:تغییرگذرواژه
Confirm password:تایید گذرواژه
Confirm:تایید
Confirmed:تایید شد
Confirmed password:گذرواژه تایید شد
Password Confirmed:گذرواژه تایید شده
Reset my password:تغییر گذرواژه‌ام
Reset my password:تغییر گذرواژه‌ی من
Reset my password:تغییر گذرواژه
Create an account:ساخت حساب
Example:مثال
Example:نمونه
No results found:نتیجه‌ای یافت نشد
Filter:فیلتر
subscriber:مشترک
subscribers:مشترکین
Change the language:تغییر زبان
Change:تغییر
newsletter:خبرنامه
local:محلی
Administration:مدیریت
Dark mode:حالت تاریک
Light mode:حالت روشن
Night Mode:حالت شبانه
Mode:حالت
Dark:تاریک
Light:روشن
Version:نگارش
Version:نسخه
Time zone:منطقه‌ زمانی
Time:زمان
zone:منطقه
Notification:آگاه‌سازی‌
Notifications:آگاه‌سازی‌ها
Notification:اعلان
Notifications:اعلانات
Message:پیام
Messages:پیام‌ها
Security:امنیت
Login verification:تایید ورود
Verify:تایید
Protect:محافظت
Information:اطلاعات
Personal:شخصی
Personal:خصوصی
Country:کشور
Timeline:خط زمان
Archive:بایگانی
Deactivate:غیرفعال
Refresh:به روزآوری
Refresh:تازه‌کردن
Help Center:مرکز راهنمایی
Terms:مقررات
Terms:شرایط
Privacy policy:سیاست حفظ حریم‌‌خصوصی
Privacy:حریم‌شخصی
Privacy:حریم‌خصوصی
policy:سیاست
Cookies:کوکی‌ها
Cookie:کوکی
Blog:بلاگ
Brand:نام‌تجاری
Apps:برنامه‌ها
App:برنامه
Jobs:فرصت‌های کاری
Jobs:شغل‌ها
Marketing:بازاریابی
Businesses:کسب و کار‌ها
Business:کسب و کار
Developers:توسعه‌دهنده‌گان
Developer:توسعه‌دهنده
Home:خانه
Tweet:توییت
Find:پیدا کردن
View:نمایش
Post:نوشته
post:مطلب
item:مورد
changes:تغییرات
Change:تغییر
Chapaer:فصل
Introducing:معرفی
Components:اجزاء
Component:جزء
Features:امکانات
Default:پیشفرض
Facilities:امکانات
Include:شامل
Support:پشتیبانی
Guide:راهنما
Figure:شکل
Online:برخط
Offline:برون‌خط
Extensive:گسترده
Files:پرونده‌ها
File:پرونده
Document:مستند
Documentation:مستندات
sign:نشانه

